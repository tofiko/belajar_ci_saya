<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index()
	{
		$this->load->view('beranda');
	}
	
	public function genap()
	{
		$this->load->view('genap');
	}
	
	public function about()
	{
		$this->load->view('contact');
	}
	
	public function contact()
	{
		$this->load->view('contact');
	}
}