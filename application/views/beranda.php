<!DOCTYPE html>
<html>
<head>
<title>Beranda</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap-responsive.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrappage.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/flexslider.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/main.css">
<!--[if lt IE 9]>
<script src="assets/js/html5.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
<style type="text/css">
#freecssfooter{display:block;width:100%;padding:120px 0 20px;overflow:hidden;background-color:transparent;z-index:5000;text-align:center;}
#freecssfooter div#fcssholder div{display:none;}
#freecssfooter div#fcssholder div:first-child{display:block;}
#freecssfooter div#fcssholder div:first-child a{float:none;margin:0 auto;}
</style></head>
<body>
<script type="text/javascript">
(function(){
  var bsa = document.createElement('script');
     bsa.type = 'text/javascript';
     bsa.async = true;
     bsa.src = '//s3.buysellads.com/ac/bsa.js';
  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);
})();
</script>
<div id="top-bar" class="container">
  <div class="row">
    <div class="span4">
      <form action="#" method="post" class="search">
        <input class="search" type="text" placeholder="Cari Kue" required>
		<input type="submit" value="Cari" name="submit">
      </form>
    </div>
    <div class="span8">
      <div class="account pull-right">
        <ul class="user-menu">
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">My Account</a></li>
          <li><a href="pages/cart.php">Your Cart</a></li>
          <li><a href="pages/checkout.php">Checkout</a></li>
          <li><a href="pages/register.php">Login</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="wrapper" class="container">
  <section class="navbar main-menu">
    <div class="navbar-inner main-menu"><a href="http://www.kue-lebaran.com/kue-lebaran-templates" class="logo pull-left"><img src="<?=base_url();?>assets/images/logo.png" class="site_logo" alt="website template image"></a>
      <nav id="menu" class="pull-right">
        <ul>
          <li><a href="pages/products.php">Terbaru</a>
            <ul>
              <li><a href="pages/products.php">Kue Lidah Kucing</a></li>
              <li><a href="pages/products.php">Kue Soes Kering</a></li>
              <li><a href="pages/products.php">Cheese Stick</a></li>
			  <li><a href="pages/products.php">Kue Kancing</a></li>
			  <li><a href="pages/products.php">Manisan Agar-Agar Kering</a></li>
			  <li><a href="pages/products.php">Kue Bangkit</a></li>
			  <li><a href="pages/products.php">Kue Truffle Cookies</a></li>
			  <li><a href="pages/products.php">Rengginang</a></li>
            </ul>
          </li>
          <li><a href="pages/products.php">Terlaris</a>
			<ul>
              <li><a href="pages/products.php">Kue Nastar</a></li>
              <li><a href="pages/products.php">Kue Putri Salju</a></li>
              <li><a href="pages/products.php">Kue Coklat Cookis</a></li>
			  <li><a href="pages/products.php">Kue Kacang</a></li>
			  <li><a href="pages/products.php">Kue Kastengel</a></li>
			  <li><a href="pages/products.php">Keripik Bawang</a></li>
			  <li><a href="pages/products.php">Astor</a></li>
			  <li><a href="pages/products.php">Kue Kembang Goyang</a></li>
            </ul>
		  </li>
          <li><a href="pages/products.php">Diskon</a>
            <ul>
              <li><a href="pages/products.php">Kue Putri Salju</a></li>
              <li><a href="pages/products.php">Kue Nastar</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </section>
  <section  class="homepage-slider" id="home-slider">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="assets/images/diskon/">
			<div class="intro1">
            <h1>Diskon Lebaran</h1>
            <p><span>Sampai Dengan 15%</span></p>
          </div>
		</li>
        <li><img src="assets/images/diskon/">
          <div class="intro2">
            <h1>Diskon Lebaran</h1>
            <p><span>Sampai Dengan 30%</span></p>
          </div>
        </li>
      </ul>
    </div>
  </section>
  <section class="main-content">
    <div class="row">
      <div class="span12">
        <div class="row">
          <div class="span12">
            <h4 class="title"><span class="pull-left"><span class="text"><span class="line">Kue <strong>Terlaris</strong></span></span></span> <span class="pull-right"><a class="left button" href="#terlaris" data-slide="prev"></a><a class="right button" href="#terlaris" data-slide="next"></a></span></h4>
            <div id="terlaris" class="terlaris slide">
              <div class="carousel-inner">
                <div class="active item">
                  <ul class="thumbnails">
                    <li class="span3">
                      <div class="product-box"><span class="sale_tag"></span>
                        <p><img src="assets/images/terlaris/"</p>
                        Kue Nastar<br>
                        <p class="price">Rp 40.000</p>
						<input type="submit" value="Beli" name="submit">
						
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box"><span class="sale_tag"></span>
                        <p><img src="assets/images/terlaris/"</p>
                        Kue Putri Salju<br>
                        <p class="price">Rp 35.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
					<li class="span3">
                      <div class="product-box"><span class="sale_tag"></span>
                        <p><img src="assets/images/terlaris/"</p>
                        Kue Coklat Cookies<br>
                        <p class="price">Rp 40.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
					<li class="span3">
                      <div class="product-box"><span class="sale_tag"></span>
                        <p><img src="assets/images/terlaris/"</p>
                        Kue Kacang<br>
                        <p class="price">Rp 25.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="item">
                  <ul class="thumbnails">
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terlaris/"</p>
                        Kue Kastengel<br>
                        <p class="price">Rp 35.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terlaris/"</p>
                        Keripik Bawang<br>
                        <p class="price">Rp 30.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terlaris/"</p>
                        Astor<br>
                        <p class="price">Rp 25.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
					  <div class="product-box">
                        <p><img src="assets/images/terlaris/"</p>
                        Kue Kembang Goyang<br>
                        <p class="price">Rp 20.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="span12">
            <h4 class="title"><span class="pull-left"><span class="text"><span class="line">Kue <strong>Terbaru</strong></span></span></span><span class="pull-right"><a class="left button" href="#myCarousel-2" data-slide="prev"></a><a class="right button" href="#myCarousel-2" data-slide="next"></a></span></h4>
            <div id="myCarousel-2" class="myCarousel carousel slide">
              <div class="carousel-inner">
                <div class="active item">
                  <ul class="thumbnails">
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Kue Lidah Kucing<br>
                        <p class="price">Rp 30.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Cheese Stick<br>
                        <p class="price">Rp 25.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Kue Kancing<br>
                        <p class="price">Rp 30.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Manisan Agar-Agar Kering<br>
                        <p class="price">Rp 25.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="item">
                  <ul class="thumbnails">
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Kue Soes Kering<br>
                        <p class="price">Rp 34.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Kue Bangkit<br>
                        <p class="price">Rp 30.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Kue Truffle Cookies<br>
                        <p class="price">Rp 40.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                    <li class="span3">
                      <div class="product-box">
                        <p><img src="assets/images/terbaru/"</p>
                        Rengginang<br>
                        <p class="price">Rp 25.000</p>
						<input type="submit" value="Beli" name="submit">
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row feature_box">
        </div>
      </div>
    </div>
  </section>
  <section id="footer-bar">
    <div class="row">
      <div class="span3">
        <h4>Navigation</h4>
        <ul class="nav">
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">Homepage</a></li>
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">About Us</a></li>
          <li><a href="pages/contact.php">Contac Us</a></li>
          <li><a href="pages/cart.php">Your Cart</a></li>
          <li><a href="pages/register.php">Login</a></li>
        </ul>
      </div>
      <div class="span4">
        <h4>My Account</h4>
        <ul class="nav">
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">My Account</a></li>
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">Order History</a></li>
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">Wish List</a></li>
          <li><a href="http://www.kue-lebaran.com/kue-lebaran-templates">Newsletter</a></li>
        </ul>
      </div>
      <div class="span5">
        <p class="logo"><img src="<?=base_url();?>assets/images/logo.png" class="site_logo" alt="website template image"></p>
        <p>Ayo Belanja Kue Lebaran mu Disini
        <br>
        <span class="social_icons"><a class="facebook" href="http://www.kue-lebaran.com/kue-lebaran-templates">Facebook</a>
    </div>
  </section>
  <section id="copyright"><span>Copyright 2015 - All right reserved.</span></section>
</div>
<script src="<?=base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>assets/js/superfish.js"></script>
<script src="<?=base_url();?>assets/js/jquery.scrolltotop1.js"></script>
<script src="<?=base_url();?>assets/js/common.js"></script>
<script src="<?=base_url();?>assets/js/jquery.flexslider-min.js"></script>
<script>$(function(){$(document).ready(function(){$(".flexslider").flexslider({animation:"fade",slideshowSpeed:4e3,animationSpeed:600,controlNav:!1,directionNav:!0,controlsContainer:".flex-container"})})});</script>
<script type="text/javascript">
var gaProperty = 'UA-159243-28';var disableStr = 'ga-disable-' + gaProperty;if (document.cookie.indexOf(disableStr + '=true') > -1) {window[disableStr] = true;}
function gaOptout(){document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2045 23:59:59 UTC; path=/';window[disableStr] = true;alert('Google Tracking has been deactivated');}
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-159243-28', 'auto');ga('set', 'anonymizeIp', true);ga('send', 'pageview');
</script>
</body>
</html>